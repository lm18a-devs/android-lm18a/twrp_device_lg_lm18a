#
# Copyright (C) 2023 TeamWin Recovery Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/twrp_lm18a.mk

COMMON_LUNCH_CHOICES := twrp_lm18a-eng
